// It should not be necessary to change this script
const createCsvWriter = require('csv-writer').createObjectCsvWriter;
const { tables, defaultTotalRows, rowsPerTable } = require("./models");

const tableNames = Object.keys(tables);
const data = {};
tableNames.forEach(async (tableName) => {
  console.log(tableName);
  const table = tables[tableName];
  const columns = Object.keys(table);
  const csvWriter = createCsvWriter({
    path: `./generated/${tableName}.csv`,
    header: columns.map(col => ({ id: col, title: col }))
  });
  const rows = [];
  const totalRows = rowsPerTable[tableName] || defaultTotalRows;
  for (let i = 0; i < totalRows; i++) {
    if (i % 1000 === 0) console.log(tableName, i);
    const row = {};
    columns.forEach(columnName => {
      const column = table[columnName];
      row[columnName] = column(data);
    });
    rows.push(row);
  }
  data[tableName] = rows;
  await csvWriter.writeRecords(rows);
});