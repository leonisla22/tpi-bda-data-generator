const faker = require("faker"); // The library used to generate fake data https://www.npmjs.com/package/faker

// Helpers variables to link data between rows or row's columns
let lastFase;
let lastDate;
let diff;
let lastProyecto;
let proyectosSinFases;
let recursosAsignados = [];
let lastInvolucrado;
let lastTrabaja;

/**
 * Define the tables required below, format:
 * const tables = {
 *    nombre_tabla: {
 *      nombre_columna: funcion
 *    }
 * };
 * 
 * NOTE: the order matters! Try to use a logical insert order when defining the tables below.
 */
const tables = {
  empleado: {
    cod_empleado: () => faker.datatype.number(10000000),
    dni: () => faker.datatype.number(50000000),
    nombre: () => faker.name.findName(),
    titulacion: () => faker.name.prefix(),
    anios_experiencia: () => faker.datatype.number(40),
    direccion: () => faker.address.streetAddress(true),
    tipo: () => faker.random.arrayElement(["jefe proyecto", "informático", null]),
  },

  jefe_proyecto: {
    cod_jefe_proyecto: ({ empleado }) => faker.random.arrayElement(empleado).cod_empleado,
  },

  informatico: {
    cod_informatico: ({ empleado }) => faker.random.arrayElement(empleado).cod_empleado,
    tipo: () => faker.random.arrayElement(["analista", "programador"]),
  },

  analista: {
    cod_analista: ({ informatico }) => faker.random.arrayElement(informatico).cod_informatico,
  },

  programador: {
    cod_programador: ({ informatico }) => faker.random.arrayElement(informatico).cod_informatico,
  },

  lenguaje: {
    cod_lenguaje: () => faker.datatype.number(100000),
    nombre: () => faker.system.commonFileExt(),
    descripcion: () => faker.lorem.words(10),
  },

  programa_en: {
    cod_programador: ({ programador }) => faker.random.arrayElement(programador).cod_programador,
    cod_lenguaje: ({ lenguaje }) => faker.random.arrayElement(lenguaje).cod_lenguaje,
  },

  proyecto: {
    cod_proyecto: () => faker.datatype.number(100000),
    nombre: () => faker.random.words(2),
    descripcion: () => faker.lorem.words(10),
    cliente: () => faker.company.companyName(),
    fecha_inicio: () => {
      lastDate = faker.date.between("2011-01-01", "2021-01-01").toISOString().split("T")[0];
      return lastDate;
    },
    fecha_fin: () => {
      const fin = faker.date.between("2021-01-01", "2031-01-01").toISOString().split("T")[0];
      diff = (new Date(fin) - new Date(lastDate)) / (1000 * 60 * 60);
      return fin;
    },
    presupuesto: () => faker.datatype.float({ min: 100000 }),
    horas_estimadas: () => faker.datatype.float({ min: 0, max: diff }),
    tipo: () => faker.random.arrayElement(["público", "privado"]),
  },

  relacionado_con: {
    cod_proyectoA: ({ proyecto }) => {
      lastProyecto = faker.random.arrayElement(proyecto);
      return lastProyecto.cod_proyecto;
    },
    cod_proyectoB: ({ proyecto }) => faker.random.arrayElement(proyecto.filter(p => p.cod_proyecto !== lastProyecto.cod_proyecto)).cod_proyecto,
  },

  fase: {
    cod_proyecto: ({ proyecto }) => {
      if (!proyectosSinFases) proyectosSinFases = proyecto.map(p => p);
      lastProyecto = faker.random.arrayElement(proyectosSinFases);
      proyectosSinFases = proyectosSinFases.filter(p => p.cod_proyecto !== lastProyecto.cod_proyecto);
      return lastProyecto.cod_proyecto;
    },
    cod_fase: () => faker.datatype.number(100000),
    nombre: () => faker.random.word(),
    fecha_inicio: () => {
      lastDate = faker.date.between(lastProyecto.fecha_inicio, lastProyecto.fecha_fin).toISOString().split("T")[0];
      return lastDate;
    },
    fecha_fin: () => faker.date.between(lastDate, lastProyecto.fecha_fin).toISOString().split("T")[0],
    estado: () => faker.random.arrayElement(["en curso", "finalizada"]),
  },

  recurso: {
    cod_id: () => faker.datatype.number(100000),
    nombre: () => faker.commerce.product(),
    descripcion: () => faker.lorem.words(10),
    tipo: () => faker.random.arrayElement(["hw", "sw"]),
  },

  se_asigna: {
    cod_proyecto: ({ fase }) => {
      lastFase = faker.random.arrayElement(fase);
      return lastFase.cod_proyecto;
    },
    cod_fase: () => lastFase.cod_fase,
    cod_id: ({ recurso }) => {
      const rec = faker.random.arrayElement(recurso.filter(r => recursosAsignados.indexOf(r.cod_id) === -1));
      recursosAsignados.push(rec.cod_id);
      return rec.cod_id;
    },
    fecha_inicio: () => {
      lastDate = faker.date.between(lastFase.fecha_inicio, lastFase.fecha_fin).toISOString().split("T")[0];
      return lastDate;
    },
    fecha_fin: () => faker.date.between(lastDate, lastFase.fecha_fin).toISOString().split("T")[0],
  },

  producto: {
    cod_pr: () => faker.datatype.number(100000),
    nombre: () => faker.commerce.product(),
    descripcion: () => faker.lorem.words(10),
    estado: () => faker.random.arrayElement(["si", "no"]),
    responsable: ({ analista }) => faker.random.arrayElement(analista).cod_analista,
    tipo: () => faker.random.arrayElement(["software", "prototipo", "informe técnico"]),
  },

  genera: {
    cod_proyecto: ({ fase }) => {
      lastFase = faker.random.arrayElement(fase);
      return lastFase.cod_proyecto;
    },
    cod_fase: () => lastFase.cod_fase,
    cod_pr: ({ producto }) => faker.random.arrayElement(producto).cod_pr,
  },

  software: {
    cod_software: ({ producto }) => faker.random.arrayElement(producto).cod_pr,
    tipo: () => faker.random.arrayElement(["diagrama", "programa", "otro"]),
  },

  prototipo: {
    cod_prototipo: ({ producto }) => faker.random.arrayElement(producto).cod_pr,
    version: () => faker.git.shortSha(),
    ubicacion: () => faker.git.commitSha(),
  },

  involucrado_en: {
    cod_proyecto: ({ fase }) => {
      lastFase = faker.random.arrayElement(fase);
      diff = (new Date(lastFase.fecha_fin) - new Date(lastFase.fecha_inicio)) / (1000 * 60 * 60);
      return lastFase.cod_proyecto;
    },
    cod_fase: () => lastFase.cod_fase,
    cod_informatico: ({ informatico }) => faker.random.arrayElement(informatico).cod_informatico,
    cod_pr: ({ producto }) => faker.random.arrayElement(producto).cod_pr,
    num_horas: () => faker.datatype.float({ min: 0, max: diff }),
  },

  trabaja: {
    cod_proyecto: ({ involucrado_en, proyecto }) => {
      lastInvolucrado = faker.random.arrayElement(involucrado_en);
      lastProyecto = proyecto.find(p => p.cod_proyecto === lastInvolucrado.cod_proyecto);
      return lastInvolucrado.cod_proyecto;
    },
    cod_informatico: () => lastInvolucrado.cod_informatico,
    num_horas: () => faker.datatype.float(),
    costo_total: () => faker.datatype.float({ max: lastProyecto.presupuesto / 100000 }),
  },

  gasto: {
    cod_gasto: () => faker.datatype.number(100000),
    cod_proyecto: ({ trabaja, proyecto }) => {
      lastTrabaja = faker.random.arrayElement(trabaja);
      lastProyecto = proyecto.find(p => p.cod_proyecto === lastTrabaja.cod_proyecto);
      return lastProyecto.cod_proyecto;
    },
    cod_empleado: () => lastTrabaja.cod_informatico,
    descripcion: () => faker.lorem.words(10),
    fecha: () => faker.date.between(lastProyecto.fecha_inicio, lastProyecto.fecha_fin).toISOString(),
    importe: () => faker.datatype.float({ max: lastProyecto.presupuesto / 100000 }),
    tipo: () => faker.random.arrayElement(["dietas", "viajes", "alojamiento"]),
  },

  dirigido: {
    cod_proyecto: ({ proyecto }) => {
      lastProyecto = faker.random.arrayElement(proyecto);
      return lastProyecto.cod_proyecto;
    },
    cod_jefe: ({ jefe_proyecto }) => faker.random.arrayElement(jefe_proyecto).cod_jefe_proyecto,
    num_horas: () => faker.datatype.float(),
    coste_total: () => faker.datatype.float({ max: lastProyecto.presupuesto / 100000 }),
  }
};

const defaultTotalRows = 10000;
// Custom amount of rows to generate per table
const rowsPerTable = {
  empleado: 100000,
  fase: 1000,
  se_asigna: 1000
};

module.exports = {
  tables,
  defaultTotalRows,
  rowsPerTable
};