# Script Generador de Datos

## Requisitos
- [git](https://git-scm.com/downloads)
- [Node.js](https://nodejs.org/en/download/)

## Como usar

1. Clonar el repositorio

```bash
git clone https://gitlab.com/leonisla22/tpi-bda-data-generator.git
```

2. Cambiar de directorio

```bash
cd tpi-bda-data-generator
```

3. Instalar paquetes

Utilizando yarn
```bash
yarn
```

Nota: para instalar yarn simplemente ejecutar
```bash
npm i -g yarn
```

4. Editar el archivo `./models.js`, ajustandolo a su escenario

5. Ejecutar el script

```bash
node ./script.js
```

6. El resultado debería encontrarse en el directorio `./generated/`, con un CSV por cada tabla definida en `./models.js`